'use strict';

/**
 * @ngdoc function
 * @name angularApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the angularApp
 */
angular.module('angularApp')
  .controller('LoginCtrl', ['$scope', '$cookies', '$http','ngDialog' , function ($scope,$cookies,$http,ngDialog) {
    var cookieName = 'uname';
    if (typeof $scope.lc !== 'undefined' &&  $cookies.get(cookieName)){
//        console.log(window.atob ( $cookies.get(cookieName) ));
        $scope.lc.username = $cookies.get(cookieName) ;
    }

    $scope.formSubmit=function()
    {
    	var user = {'username':$scope.lc.username,'password':$scope.lc.password};
    	var remember = $scope.lc.rememberMe;
    	if (remember){
    	  $cookies.put(cookieName,$scope.lc.username);
    	}
    	console.log($cookies.get(cookieName));

    	$http
    	(
    	  {
    	    'method':'POST',
    	    'url' : 'test/authenticate',
    	    'data':user
     	  }
    	).then(function successCallback(response){
            console.log(response);
        },function errorCallback(response){
            console.log(response);
        }
    	);
    };
    $scope.userSignUp = function ()
    {
      ngDialog.openConfirm({ template:'views/userSignUp.html',className:'ngdialog-theme-default'});
    };
  }]);
